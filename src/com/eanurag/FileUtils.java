package com.eanurag;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class FileUtils {

    private static boolean successFlag = false;


    public static void printAttributes(Path filePath) throws IOException {
        Files.setLastModifiedTime(filePath, FileTime.from(getNewInstantOfTime()));
        out.println("new time: " + Files.getLastModifiedTime(filePath));
    }


    private static Instant getNewInstantOfTime() {
        LocalDateTime dateTime = LocalDateTime.of(2018, Month.FEBRUARY, 12, 23, 47, 10);
        return dateTime.toInstant(ZoneOffset.of("+0530"));
    }

//    public static void main(String[] args) throws IOException {
//
//        Path path = Paths.get("C:\\Users\\anurag.upadhaya\\Documents\\Test\\[fmovies.to]BoneTomahawk+HD1080p.mp4");
//
//        printAttributes(path);

//    }

    public static void modifyFileMeta(List<String> files, LastModified lastModified) {
        try {

            files.forEach(file -> {
                File filez = new File(file);
                if (filez.isFile()) {
                    if (filez.exists()) {
                        String absolutePath = filez.getAbsolutePath();
                        Path path = Paths.get(absolutePath);


                        LocalDateTime dateTime = LocalDateTime.of(Integer.parseInt(lastModified.getYear()), Month.valueOf(lastModified.getMonth().toUpperCase()), Integer.parseInt(lastModified.getDate()), Integer.parseInt(lastModified.getHour()), Integer.parseInt(lastModified.getMinute()), Integer.parseInt(lastModified.getSecond()));
                        Instant instant = dateTime.toInstant(ZoneOffset.of("+0530"));
                        try {
                            Files.setLastModifiedTime(path, FileTime.from(instant));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            successFlag = true;

        } catch (Exception e) {
            System.out.println("Something went horribly wrong: " + e.getMessage());
            successFlag = false;
        } finally {
            displayMessage(successFlag);
        }

    }

    public static List<String> getFileNames() {

        List<String> files = new ArrayList<>();

        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.setDialogTitle("Choose files to rename: ");
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setMultiSelectionEnabled(true);
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            java.io.File[] selectedFiles = jfc.getSelectedFiles();
            Arrays.asList(selectedFiles).forEach(x -> {
                files.add(x.getAbsolutePath());
            });
        }

        return files;
    }

    private static void displayMessage(boolean successFlag) {
        JOptionPane.showMessageDialog(null, successFlag ? "Success" : "Failed");
    }
}
