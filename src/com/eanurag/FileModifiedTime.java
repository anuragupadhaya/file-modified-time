package com.eanurag;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class FileModifiedTime {

    private static JFrame frame;

    private JComboBox comboBoxYear;
    private JComboBox comboBoxMonth;
    private JComboBox comboBoxDay;
    private JComboBox comboBoxHour;
    private JComboBox comboBoxMin;
    private JComboBox comboBoxSec;
    private JLabel secondLabel;
    private JLabel yearLabel;
    private JLabel monthLabel;
    private JPanel fileMetaPanel;
    private JLabel hourLabel;
    private JLabel minLabel;
    private JLabel dayLabel;
    private JList filesList;
    private JLabel filesLabel;
    private JButton chooseFilesButton;
    private JButton removeButton;
    private JButton resetButton;
    private JButton startButton;

    public FileModifiedTime() {

        chooseFilesButton.addActionListener(e -> {
            List<String> files = FileUtils.getFileNames();
            DefaultListModel<String> demoList = new DefaultListModel<>();
            filesList.setModel(demoList);
            files.forEach(x -> {
                demoList.addElement(x);
            });
            frame.pack();
        });

        resetButton.addActionListener(e -> {
            DefaultListModel model = (DefaultListModel) filesList.getModel();
            model.removeAllElements();
            frame.pack();
        });

        removeButton.addActionListener(e -> {

            if (filesList.getSelectedIndex() >= 0) {
                DefaultListModel model = (DefaultListModel) filesList.getModel();
                model.remove(filesList.getSelectedIndex());
            }
            frame.pack();
        });

        startButton.addActionListener(e -> {
            LastModified lastModified = new LastModified((String) comboBoxYear.getSelectedItem(),
                    (String) comboBoxMonth.getSelectedItem(),
                    (String) comboBoxDay.getSelectedItem(),
                    (String) comboBoxHour.getSelectedItem(),
                    (String) comboBoxMin.getSelectedItem(),
                    (String) comboBoxSec.getSelectedItem());


            List<String> files = new ArrayList<>();
            for (int i = 0; i < filesList.getModel().getSize(); i++) {
                files.add((String) filesList.getModel().getElementAt(i));
            }

            FileUtils.modifyFileMeta(files, lastModified);

        });
    }

    public static void main(String[] args) {

        setAppTheme();

        frame = new JFrame("File Modified Time");
        frame.setContentPane(new FileModifiedTime().fileMetaPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private static void setAppTheme() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
